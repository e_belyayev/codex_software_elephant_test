from django.contrib import admin
from Accounts.models import Manager, Developer
admin.site.register(Manager)
admin.site.register(Developer)
