
# TODO create_canvas 
# TODO create_line
# TODO create_rectangle
# TODO bucket_fill
from os import linesep

class Drawing:
    def __init__(self, w, h, color='o'):
        try:
            self.width = int(w)
            self.height = int(h)
            self.fill = color
        except ValueError:
            print("You have provided a wrong width or height")


    def save_to_file(self, filename, format='txt'):

        self.file = '{}.{}'.format(filename, format)
        with open(self.file, 'a') as f:
            f.write(self.canvas)
        print(self.__str__() + 'was saved as {}...'.format(self.file))

    def __str__(self):

        return "Image with width={}, height={}, and color={}".format(
            self.width, self.height, self.fill)
    
    def create_canvas(self):
        self.top_boarder = '-'*(self.width+2)+linesep
        self.canvas = ''
        self.canvas += self.top_boarder
        self.canvas += ''.join(['|'+' '*(self.width)+'|'+linesep for h in range(self.height)])
        self.canvas += self.top_boarder

        return self.canvas

        
    def create_line(self, x1, y1, x2, y2):
        x1, y1, x2, y2 = map(int, (x1, y1, x2, y2,))
        if self.canvas:
            self.canvas = self.canvas.split(linesep)
            if y2-y1 == 0:
                line = self.canvas[y1]
                line = line[0:x1]+'x'*(x2+1-x1)+line[x2+1:]
                self.canvas[y1]=line
                
            else:
                for index in range(y1,y2+1):
                    line = self.canvas[index]
                    line = line[0:x1] + 'x' + line[x2+1:]
                    self.canvas[index]=line
            self.canvas = linesep.join(self.canvas)
            return self.canvas          
        else:
            canvas = [''.rjust(self.width)+linesep for h in range(self.height+2)]
            if y2-y1 == 0:
                line = canvas[y1]
                line = line[x1]+'x'*(x2+1-x1)+line[x2:]
                canvas[y1]=line
                return ''.join(canvas)
        

    def create_rectangle(self, x1, y1, x2, y2):
        if self.canvas:
            x1, y1, x2, y2 = map(int, (x1, y1, x2, y2,))
            if self.canvas:
                self.canvas = self.canvas.split(linesep)
                self.canvas[y1] = self.canvas[y1][0:x1-2]+ 'x'*(x2+1-x1)+self.canvas[y1][x2-1:]
                self.canvas[y2] = self.canvas[y2][0:x1-2]+ 'x'*(x2+1-x1)+self.canvas[y2][x2-1:]
                for index in range(y1+1,y2):
                    line = self.canvas[index]
                    self.canvas[index] = line[0:x1-2]+'x'+line[x1:x2-1]+'x'+line[x2-1:]
                self.matrix = [list(line+linesep) for line in self.canvas]
                self.canvas = linesep.join(self.canvas)
                return self.canvas
                

            
        
    def bucket_fill(self, x1, y1, fill):
        if self.canvas:
            x, y = map(int, (x1,y1))
            fill = fill.strip()
            try:
                if self.matrix[y][x] == " ":  
                    self.matrix[y][x] = fill
                    if x >= 0 and x<=20:
                        self.bucket_fill(x-1,y, fill)
                    if x < 20 :
                        self.bucket_fill(x+1,y, fill)
                    if y >=  0 and y<=len(self.matrix):
                        self.bucket_fill(x,y-1, fill)
                    if y <= len(self.matrix):
                        self.bucket_fill(x,y+1, fill)
            except IndexError:
                pass
            self.canvas = ''.join([''.join(line) for line in self.matrix])
            return self.canvas


                
              

if __name__ == '__main__':
   
    with open('input.txt') as f:
        for line in f.readlines():
            line = line.split(' ')
            if line[0] == 'C':
                draw = Drawing(line[1], line[2])
                print(draw.create_canvas())
            elif line[0] == 'L':
                print(draw.create_line(line[1], line[2], line[3], line[4]))
            elif line[0] == 'R':
                print(draw.create_rectangle(line[1], line[2], line[3], line[4]))
            elif line[0] == 'B':
                print(draw.bucket_fill(line[1], line[2], line[3]))
